
// variables

const allDiv = document.getElementById("all-tasks");
const activeDiv = document.getElementById("active-tasks");
const completedDiv = document.getElementById("completed-tasks");
const InputBarDiv = document.getElementById("input_bar");


let flag = true;

if (flag === true) {
  showAll();
  flag = false;
}


function showAll() {

  allDiv.style.display = 'block';
  activeDiv.style.display = 'none';
  completedDiv.style.display = 'none';
  InputBarDiv.style.display = 'block';

  const completedButton = document.getElementById("completed-btn");
  completedButton.classList.remove("tab");

  const activeButton = document.getElementById("active-btn");
  activeButton.classList.remove("tab");

  const allButton = document.getElementById("all-btn");
  allButton.classList.add("tab");

}

function showActive() {

  allDiv.style.display = 'none';
  activeDiv.style.display = 'block';
  completedDiv.style.display = 'none';
  InputBarDiv.style.display = 'block';

  const completedButton = document.getElementById("completed-btn");
  completedButton.classList.remove("tab");

  const activeButton = document.getElementById("active-btn");
  activeButton.classList.add("tab");

  const allButton = document.getElementById("all-btn");
  allButton.classList.remove("tab");

}

function showCompleted() {

  allDiv.style.display = 'none';
  activeDiv.style.display = 'none';
  completedDiv.style.display = 'block';
  InputBarDiv.style.display = 'none';

  const completedButton = document.getElementById("completed-btn");
  completedButton.classList.add("tab");


  const activeButton = document.getElementById("active-btn");
  activeButton.classList.remove("tab");

  const allButton = document.getElementById("all-btn");
  allButton.classList.remove("tab");


}


//================================

const inputBar = document.getElementById("addInput");

inputBar.addEventListener("keypress", (event) => {
  if (event.key === "Enter") {
    addTodo();
  }
})

function addTodo() {

  const todoTask = document.getElementById("addInput").value;

  const id = new Date().getTime();

  if (todoTask.length >= 1) {

    appendActiveTask(todoTask, id);

    appendAllTask(todoTask, id);

    document.getElementById("addInput").value = "";

  }

  // updating checkedd status of
  const completedTaskIds = document.getElementById('completed-tasksdiv').children;

  if (completedTaskIds.length > 0) {

    for (let index = 0; index < completedTaskIds.length; index++) {

      let completedID = completedTaskIds[index]["id"];

      let allId = completedID.substring(13);

      let checkbox = document.getElementById(`allTSK1${allId}`);
      checkbox.checked = true;
    }
  }

}


function appendActiveTask(task, id) {

  let active = document.getElementById("active-tasks");

  active.innerHTML += `
     <div  id = "active${id}"  class="taskss">
      <input class = "checkBox" type="checkbox" id="TSK" onclick="applyCheckToActiveTab(${id})" ></input>
      <label id = "activeTask${id}"><span>${task}</span></label>     
 </div>`

}

function appendAllTask(task, id) {

  let all = document.getElementById("all-tasks");

  all.innerHTML += `
     <div id = "all-task${id}" class="taskss">
     <input class = "checkBox" id="allTSK1${id}" type="checkbox" onclick="applyCheckToAllTab(${id})" ></input>
      <label id = "allTask${id}"><span>${task}</span></label>     
 </div>`

}

function appendCompletedTask(task, id) {

  let completed = document.getElementById(`completed-tasksdiv`)

  completed.innerHTML += `
     <div id = "completedtask${id}" class="ct" >
     <div class = "comptask">
     <input class = "checkBox" id="cTSK1${id}" type="checkbox" onclick="applyCheckToCompletedTab(${id})" ></input>
     <label id = "cTask${id}"><span>${task}</span></label>
     </div>
     <div class = "deleteContainer"
        <button onclick="deleteTodo(${id})" id="deleteButton"><i class="fa fa-trash" aria-hidden="true"></i></button> 
        </div> 
 </div>`


  let completedLabel = document.getElementById(`cTask${id}`)

  completedLabel.classList.add("chk");

  let completedCB = document.getElementById(`completedtask${id}`);
  completedCB.checked = true;

  // ======================================

  const completedTaskIds = document.getElementById('completed-tasksdiv').children;

  if (completedTaskIds.length > 0) {

    for (let i = 0; i < completedTaskIds.length; i++) {

      let completedID = completedTaskIds[i]["id"];

      let allId = completedID.substring(13);

      let checkbox = document.getElementById(`cTSK1${allId}`);
      checkbox.checked = true;
    }
  }

  // =============================================

}


//===============================

function applyCheckToActiveTab(id) {

  let allRemove = document.getElementById(`allTask${id}`);

  allRemove.classList.add("chk");

  let activeRemove = document.getElementById(`active${id}`);

  activeRemove.remove();

  let completedAppend = document.getElementById(`all-task${id}`).innerText;

  appendCompletedTask(completedAppend, id);

  let checkbox = document.getElementById(`allTSK1${id}`);
  checkbox.checked = true;

}

function applyCheckToAllTab(id) {

  let task = document.getElementById(`all-task${id}`).innerText;

  let checkbox = document.getElementById(`allTSK1${id}`);

  let completeRemove = document.getElementById(`completedtask${id}`);

  if (checkbox.checked === false) {

    completeRemove.remove();

    appendActiveTask(task, id);

    let allRemove = document.getElementById(`allTask${id}`);

    allRemove.classList.remove("chk");

  } else {

    applyCheckToActiveTab(id);
  }
}

function applyCheckToCompletedTab(id) {

  let getTask = document.getElementById(`cTask${id}`).innerText;

  appendActiveTask(getTask, id);

  let completedTaskDiv = document.getElementById(`completedtask${id}`);

  completedTaskDiv.remove();

  let allt = document.getElementById(`allTSK1${id}`);

  allt.checked = false;

  let aladadlt = document.getElementById(`allTask${id}`);
  aladadlt.classList.remove("chk");

}

function deleteTodo(id) {

  const completedDiv = document.getElementById(`completedtask${id}`);

  const allDiv = document.getElementById(`all-task${id}`);

  allDiv.remove();

  completedDiv.remove();

}

function deleteAllTodo() {

  const allIds = document.getElementById('completed-tasksdiv').children

  for (let index = allIds.length - 1; index >= 0; index--) {

    let completedID = allIds[index]["id"];

    let allId = completedID.substring(13);

    allId = "all-task" + allId;

    let completedDiv = document.getElementById(completedID);

    let allDiv = document.getElementById(allId);

    completedDiv.remove();
    allDiv.remove();

  }

}