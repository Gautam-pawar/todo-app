# ToDO app

## Hosting Link : https://gautam-todo-application.netlify.app/

## Technologies used in project 


* HTML
* CSS
* JavaScript 


## Description 

Use / Application: Noting down the tasks to be done and marking the status as "active" or "finished.

The application has three tabs: 
1. All tasks: Displays all of the tasks, both active and completed.

2. Active: Displays all tasks that are currently underway.

3. completed: Displays all of the tasks that have been finished.

